const { Router } = require("express")
const controller = require('./controller');

const router = Router();

router.get('/', controller.getAllAnime);
router.post('/', controller.postAnimeList);

module.exports = router