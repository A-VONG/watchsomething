const pool = require('../../db');
//Recupere tout les animes dans la bdd
const getAllAnime = ( req, res) => {
    pool.query('SELECT * FROM public."Anime"', (error, results) => {
        if(error) throw error;
        res.status(200).json(results.rows)
    })
}
// Met tout les animes dans la BDD
const postAnimeList = (req, res) => {
    pool.query(`DELETE FROM public."Anime" WHERE id>=0`)
    for(let i=0; i<req.body.length; i++) {
        let name = req.body[i]
        if(name.includes(`'`)){
            name = name.replaceAll(`'`, ` `)
            console.log(name)
        }
        pool.query(`INSERT INTO public."Anime"(name,id) VALUES('${name}','${i}')`);
    }
}

module.exports = {
    getAllAnime,
    postAnimeList
}