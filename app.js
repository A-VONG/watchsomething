var express = require('express');
var app = express();
var cors = require('cors')
var port = "8080"
const AnimeRouter = require('./routes/anime/routes.js');

app.use(cors());
app.use(express.json());

app.use('/Anime', AnimeRouter);

app.listen(port, function () {
  console.log('Example app listening on port '+port+'!');
});